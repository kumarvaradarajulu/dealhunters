# Create your views here.
from deals.models import Deals, Comments, Votes
from django.contrib import auth
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render_to_response, render
from bod.forms import MyRegistrationForm
from django.core.urlresolvers import reverse
from django.core.context_processors import csrf
from common.frequentfuncs import get_create_user, bbcode_func, get_client_ip, hashit
from itertools import chain
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.core.validators import validate_email
import json
import datetime
from django.utils.timezone import now

def postcomment(request, post_type, post_id):
    json_response = {}
    if request.method == 'POST':
        comment_data = request.POST.get('markItUp', '')
        if not comment_data:
            json_response['status'] = 'f'
            json_response['message'] = 'Comment Cannot be blank'
            return HttpResponse(json.dumps(json_response), content_type='application/json')

        if not request.user.is_authenticated:
            email = request.POST.get('email', '')
            try:
                validate_email(email)
                user = get_create_user(email, request)
            except ValidationError:
                json_response['status'] = 'f'
                json_response['message'] = 'Enter valid email address'
                return HttpResponse(json.dumps(json_response), content_type='application/json')
        else:
            user = request.user
        try:
            post = Deals.objects.get(pk=post_id)
            comment = Comments()
            comment.deal_id = post
            comment.comment = comment_data
            comment.likes = comment.unlikes = comment.abusive = 0
            comment.visible = False
            comment.date_time = now()
            comment.user = user
            comment.save()
            json_response['status'] = '1'
            json_response['message'] = 'Thanks for your comment. Your comment will appear shortly after moderation...'
            return HttpResponse(json.dumps(json_response), content_type='application/json')
        except ObjectDoesNotExist:
            json_response['status'] = ''
            json_response['message'] = 'OOPS, an internal Error occured. Please try again'
            return HttpResponse(json.dumps(json_response), content_type='application/json')


def vote(request, post_type):
    json_response = dict()
    if not request.user.is_authenticated():
        json_response['status'] = 'f'
        json_response['message'] = 'Please <a href="%s">Login/Register</a>' % reverse('login')
        return HttpResponse(json.dumps(json_response), content_type='application/json')

    if request.method == 'POST' or request.method == 'AJAX':
        vote_type = request.POST.get('vote_type', '')
        post_id = request.POST.get('post_id', '')

        if vote_type == 'up':
            # TODO: This voting logic needs to be changed to incorporate a broader mechanism
            numvotes = 1
            votetype = 0
        elif vote_type == 'down':
            numvotes = -1
            votetype = 1
        else:
            json_response['status'] = 'f'
            json_response['message'] = 'Internal error occurred. Try again later...'
            return HttpResponse(json.dumps(json_response), content_type='application/json')
        ip = hashit(get_client_ip(request), request.user.username)
        try:
            vote = Votes.objects.get(ip=ip, deal_id=post_id)
            json_response['status'] = 'f'
            json_response['message'] = 'Your vote has already been registered!'
            return HttpResponse(json.dumps(json_response), content_type='application/json')

        except ObjectDoesNotExist:
            try:
                deal = Deals.objects.get(pk=post_id)
                vote = Votes()
                vote.deal_id = deal
                vote.votetype = votetype
                vote.user = request.user
                vote.ip = ip
                vote.date_time = now()
                vote.save()
                deal.temperature = deal.temperature + numvotes
                print "num votes : ", numvotes
                deal.save()
            except ObjectDoesNotExist:
                json_response['status'] = 'f'
                json_response['message'] = 'OOPS, an internal Error occured. Please try again'
                return HttpResponse(json.dumps(json_response), content_type='application/json')

        json_response['status'] = ''
        json_response['message'] = 'Thanks for Casting your vote.'
        json_response['temp'] = deal.temperature
        return HttpResponse(json.dumps(json_response), content_type='application/json')