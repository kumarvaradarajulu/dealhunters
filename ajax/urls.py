from django.conf.urls import url, patterns


urlpatterns = patterns(
    '',
    url(r'^postcomment/(?P<post_type>[a-zA-Z]+)/(?P<post_id>[0-9]+)$', 'ajax.views.postcomment', name='postcomment'),
    url(r'^vote/(?P<post_type>[a-zA-Z]+)$', 'ajax.views.vote', name='vote'),
)
