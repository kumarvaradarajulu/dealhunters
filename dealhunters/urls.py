from django.conf.urls import patterns, include, url
from django.conf import settings

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'bod.views.home', name='home'),
    # url(r'^bod/', include('bod.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^blog/', include('blog.urls', namespace='blog')),
    url('^help', 'bod.views.gethelp', name='help'),
    url('^about', 'bod.views.about', name='about'),
    url('^user', 'userprofile.views.userdetails', name='userdetails'),
    url('^vote', include('vote.urls', namespace='vote')),
    url('^feedback', 'bod.views.feedback', name='feedback'),
    url('^login', 'bod.views.login', name='login'),
    url('^logout', 'bod.views.logout', name='logout'),
    #url('^loggedin$', 'bod.views.loggedin'),
    #url('^loggedout$', 'bod.views.loggedout'),
    url('^logininvalid$', 'bod.views.logininvalid'),
    url('^auth$', 'bod.views.auth_view'),
    url('^register$', 'bod.views.register', name='register'),
    url('^register_success$', 'bod.views.register_success', name='register_success'),
    url(r'^deals/', include('deals.urls', namespace='deals')),
    url(r'^submit$', 'deals.views.submit', name='submit'),
    url(r'^asja/', include('ajax.urls', namespace='ajax')),
    url(r'^$', 'deals.views.dealslist', name='index'),
)


if settings.DEBUG:
    # static files (images, css, javascript, etc.)
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}))