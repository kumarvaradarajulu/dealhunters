__author__ = 'kumar'

from django.contrib import auth
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, render
from bod.forms import MyRegistrationForm
from django.core.urlresolvers import reverse
from django.core.context_processors import csrf
from common.frequentfuncs import get_sidebar, populate_categories, common_funcs
from itertools import chain

def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('index'))

    args = {}
    params = {}
    params = dict(chain(params.iteritems(), populate_categories(None, False).iteritems()))
    params['user'] = request.user if request.user.is_authenticated() else ''
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)

        if user:
            auth.login(request, user)
            return HttpResponseRedirect(request.META.get('HTTP_REFERER', reverse('index')))
        else:
            args['login_error'] = True
            args['reg_form'] = MyRegistrationForm()
    else:
        args['reg_form'] = MyRegistrationForm()

    #Get side bar
    params['sidebar'] = get_sidebar(False, False)
    args['params'] = params

    args.update(csrf(request))
    return render_to_response('register.html', args)


def logout(request):
    if request.user.is_authenticated():
        auth.logout(request)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', reverse('index')))
    else:
        return HttpResponseRedirect(reverse('login'))


def register(request):

    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('index'))

    args = {}
    params = {}
    params = dict(chain(params.iteritems(), populate_categories(None, False).iteritems()))
    params['user'] = request.user if request.user.is_authenticated() else ''
    args['reg_error'] = False
    if request.method == 'POST':
        registerform = MyRegistrationForm(request.POST)
        if registerform.is_valid():
            registerform.save()
            return HttpResponseRedirect(reverse('register_success'))
        else:
            args['reg_error'] = True
    else:
        registerform = MyRegistrationForm()

    #Get side bar
    params['sidebar'] = get_sidebar(False, False)
    args['params'] = params
    args.update(csrf(request))
    args['reg_form'] = registerform

    return render_to_response('register.html', args)


def register_success(request):
    (args, params) = common_funcs(request)
    return render_to_response('register_success.html', args)


def logininvalid(request):
    pass


def auth_view(request):
    pass


def gethelp(request):
    (args, params) = common_funcs(request)
    return render_to_response('help.html', args)


def about(request):
    (args, params) = common_funcs(request)
    return  render_to_response('about.html', args)


def feedback(request):
    (args, params) = common_funcs(request)
    return render_to_response('feedback.html', args)
