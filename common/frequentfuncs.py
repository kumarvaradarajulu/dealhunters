__author__ = 'kumar'
from datetime import date, timedelta
from deals.models import Deals, Tags, DealTags, Params
from itertools import chain
from django.core.context_processors import csrf
from django.contrib import auth
from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
import re
from django import template
from django.utils.safestring import mark_safe
from django.utils.html import conditional_escape
import hashlib

SORT_ORDER = ['date', 'hot', 'discussed']


def validate_category(category):
    for one_category in Deals.DEAL_CATEGORY:
        if one_category[1].lower() == category:
            return one_category[0]
    return False


def category_words(cat_num):
    for one_category in Deals.DEAL_CATEGORY:
        if one_category[0] == cat_num:
            return one_category[1]
    return False


def buildtitle(strings):
    """
        This function formats Title of a page. It follows commonly used SEO rule sets to format the title
        Reference: http://searchenginewatch.com/article/2154469/How-to-Write-Title-Tags-For-Search-Engine-Optimization
    """


def paginate(page, numrows, items_per_page=10, disp_ellipsis=True, num_page_links=10):
    """
        ./manage.py shell

        from common.frequentfuncs import *

        a = paginate(3, 23, 3, True)
        b = paginate(3, 300, 3, True)
        c = paginate(1, 23, 10, True)
        d = paginate(20, 200, 10, True)
        e = paginate(9, 200, 10, True)
        f = paginate(12, 200, 10, True)
        a
        b
        c
        d
        e
        f
    """
    if not page or not numrows:
        return False

    totpages = int(numrows / items_per_page) + 1 if numrows % items_per_page !=0 else numrows / items_per_page
    paginations = {}
    paginations['prev'] = []
    paginations['next'] = []
    paginations['after_ellips'] = []
    paginations['before_ellips'] = []
    paginations['leftellips'] = True
    paginations['rightellips'] = True
    paginations['totpages'] = totpages

    for count, i in enumerate(range(page-1, 0, -1)):
        if count < 3:
            paginations['prev'].append(i)

    for count, i in enumerate(range(page+1, totpages+1)):
        if count < 3:
            paginations['next'].append(i)

    if paginations['next']:
        if paginations['next'][-1] < totpages - 5:
            for count, i in enumerate(range(totpages-2, totpages+1)):
                if count < 3:
                    paginations['after_ellips'].append(i)
        else:
            paginations['rightellips'] = False
            if paginations['next']:
                for count, i in enumerate(range(paginations['next'][-1]+1, totpages+1)):
                    if count < 3:
                        paginations['next'].append(i)
    else:
        paginations['rightellips'] = False

    if paginations['prev']:
        if paginations['prev'][-1] > 5:
            for count, i in enumerate(range(3, 0, -1)):
                if count < 3:
                    paginations['before_ellips'].append(i)
        else:
            paginations['leftellips'] = False
            if paginations['next']:
                for count, i in enumerate(range(paginations['next'][-1]-1, 0)):
                    if count < 3:
                        paginations['prev'].append(i)
    else:
        paginations['leftellips'] = False

    return paginations


def get_sidebar(cat_num, auto=False):
    """
        Input:
            cat_num: Category number for which sidebar needs to be fetched.
            auto: If set to true category will be read direcly from the cookie
    """
    # Get hot deals today
    sidebar = {}

    one_day_before = date.today() - timedelta(days=1)
    one_week_before = date.today() - timedelta(days=7)
    one_month_before = date.today() - timedelta(days=30)
    six_months_before = date.today() - timedelta(days=180)

    # TODO: cat_num logic should be enhanced to support automatic getting of category from cookies using auto input
    if cat_num:
        sidebar['hottoday'] = Deals.objects.filter(category=cat_num, date_time__gte=one_day_before,
                                                   expired=False).order_by('-temperature', '-date_time')[0:10]

        sidebar['hotweekly'] = Deals.objects.filter(category=cat_num, date_time__gte=one_week_before,
                                                    expired=False).order_by('-temperature', '-date_time')[0:10]

        sidebar['hotmonthly'] = Deals.objects.filter(category=cat_num, date_time__gte=one_month_before,
                                                     expired=False).order_by('-temperature', '-date_time')[0:10]

        sidebar['hotalltime'] = Deals.objects.filter(category=cat_num, date_time__gte=six_months_before,
                                                     expired=False).order_by('-temperature', '-date_time')[0:10]
    else:
        sidebar['hottoday'] = Deals.objects.filter(date_time__gte=one_day_before,
                                                   expired=False).order_by('-temperature', '-date_time')[0:10]

        sidebar['hotweekly'] = Deals.objects.filter(date_time__gte=one_week_before,
                                                    expired=False).order_by('-temperature', '-date_time')[0:10]

        sidebar['hotmonthly'] = Deals.objects.filter(date_time__gte=one_month_before,
                                                     expired=False).order_by('-temperature', '-date_time')[0:10]

        sidebar['hotalltime'] = Deals.objects.filter(date_time__gte=six_months_before,
                                                     expired=False).order_by('-temperature', '-date_time')[0:10]

    sidebar['hotweekly'] = sidebar['hotweekly'] if sidebar['hotweekly'].count < 10 else sidebar['hotmonthly']
    sidebar['hotmonthly'] = sidebar['hotmonthly'] if sidebar['hotmonthly'].count < 10 else sidebar['hotmonthly']
    sidebar['hottoday'] = sidebar['hottoday'] if sidebar['hottoday'].count < 10 else sidebar['hotweekly']

    return sidebar


def populate_categories(category=None, auto=False):
    # TODO: This function needs to be enhanced to support categories from cookies

    temp = {}
    temp['categories'] = Deals.DEAL_CATEGORY

    if not category:
        category = 'all topics'

    # Check if the Category Matches
    if category and category.lower() != 'all topics':
        cat_num = validate_category(category)
        if cat_num:
            temp['category'] = category
    else:
        temp['category'] = 'All Topics'

    return temp


def common_funcs(request):
    args = {}
    params = get_footer(request)
    params['sidebar'] = get_sidebar(False, False)
    params['fullpath'] = request.path
    params['path_with_query'] = request.get_full_path()
    params = dict(chain(params.iteritems(), populate_categories(None, False).iteritems()))
    params['user'] = request.user if request.user.is_authenticated() else ''
    params['last_id'] = Deals.objects.latest('id').id
    args['params'] = params
    args.update(csrf(request))
    return (args, params)


def common_deal_funcs(request):
    args = {}
    params = {}
    params = get_footer(request)
    params['fullpath'] = request.path
    params['path_with_query'] = request.get_full_path()
    args.update(csrf(request))
    params['categories'] = Deals.DEAL_CATEGORY
    params['user'] = request.user if request.user.is_authenticated() else ''
    params['last_id'] = Deals.objects.latest('id').id
    return (args, params)

def get_create_user(data, request):
    """
        Input:
            data: It can be a value or a dict
                  If it is a dict all values from the dict will be extracted and used
                  If it is a value, it must be the email address
            request:
    """
    if isinstance(data, dict):
        email = data['email']
    else:
        email = data

    try:
        user = User.objects.get(email=email)
        return user
    except ObjectDoesNotExist:
        if email == data:
            user_data = User()
            user_data.email = email
            user_data.username = email.split('@')[0]
            user_data.password = make_password(user_data.username)
            user_data.save()
            # TODO: Default password is username needs to be changed to something with security
            user = auth.authenticate(username=user_data.username, password=user_data.username)
            auth.login(request, user)
            return request.user

def get_create_tags(tags, post_type='deals'):
    """
        Input:
            data: It can be a value or a dict
                  If it is a dict all values from the dict will be extracted and used
                  If it is a value, it must be the email address
            request:

        Tag Regex:
            tagreg = r'^[a-zA-Z_-\s]+'
    """
    tagreg = re.compile(r'^[\w\s]+$')
    alltagobjs = list()
    print tags.split(',')
    for tag in tags.split(','):
        tag = tag.strip()
        if tagreg.match(tag):
            try:
                Tag = Tags.objects.get(name=tag)
                Tag.tagged += 1
            except ObjectDoesNotExist:
                Tag = Tags()
                Tag.name = tag
                Tag.tagged = 0
            Tag.save()

            alltagobjs.append(Tag)

    return alltagobjs


def save_tags(deal_tag_obj, deal_obj, tag_obj, post_type):
    deal_tag_obj.tag_id = tag_obj
    deal_tag_obj.deal_id = deal_obj
    deal_tag_obj.save()


def bbcode_func(value):
    bbdata = [
        (r'\[url\](.+?)\[/url\]', r'<a href="\1">\1</a>'),
        (r'\[url=(.+?)\](.+?)\[/url\]', r'<a href="\1">\2</a>'),
        (r'\[email\](.+?)\[/email\]', r'<a href="mailto:\1">\1</a>'),
        (r'\[email=(.+?)\](.+?)\[/email\]', r'<a href="mailto:\1">\2</a>'),
        (r'\[img\](.+?)\[/img\]', r'<img src="\1">'),
        (r'\[img=(.+?)\](.+?)\[/img\]', r'<img src="\1" alt="\2">'),
        (r'\[b\](.+?)\[/b\]', r'<b>\1</b>'),
        (r'\[i\](.+?)\[/i\]', r'<i>\1</i>'),
        (r'\[u\](.+?)\[/u\]', r'<u>\1</u>'),
        (r'\[quote\](.+?)\[/quote\]', r'<div style="margin-left: 1cm">\1</div>'),
        (r'\[center\](.+?)\[/center\]', r'<div align="center">\1</div>'),
        (r'\[code\](.+?)\[/code\]', r'<tt>\1</tt>'),
        (r'\[big\](.+?)\[/big\]', r'<big>\1</big>'),
        (r'\[small\](.+?)\[/small\]', r'<small>\1</small>'),
        (r'\[quote=(.+?)\]',r'<div class="bbcode-quote"><div class="bbcode-quote-head">\1</div>'),
        (r'\[bbody\](.*?)\[/bbody\]', r'<div class="bbcode-quote-body">\1</div>'),
        (r'\[/quote\]', r'</div>'),
        ]

    for bbset in bbdata:
        p = re.compile(bbset[0], re.DOTALL)
        value = p.sub(bbset[1], value)

    #The following two code parts handle the more complex list statements
    temp = ''
    p = re.compile(r'\[list\](.+?)\[/list\]', re.DOTALL)
    m = p.search(value)
    if m:
        items = re.split(re.escape('[*]'), m.group(1))
        for i in items[1:]:
            temp = temp + '<li>' + i + '</li>'
        value = p.sub(r'<ul>'+temp+'</ul>', value)

    temp = ''
    p = re.compile(r'\[list=(.)\](.+?)\[/list\]', re.DOTALL)
    m = p.search(value)
    if m:
        items = re.split(re.escape('[*]'), m.group(2))
        for i in items[1:]:
            temp = temp + '<li>' + i + '</li>'
        value = p.sub(r'<ol type=\1>'+temp+'</ol>', value)

    return value


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR', '')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def hashit(value, salt):
    return hashlib.sha1(value + salt).hexdigest()


def get_footer(request):
    params = dict()
    params['footer'] = dict()
    params['footer']['users_online'] = Params.objects.get(key='usersonline').int1
    params['footer']['max_users_online'] = Params.objects.get(key='maxusersonline').int1
    params['footer']['total_posts'] = Deals.objects.count()
    params['footer']['total_reg_users'] = User.objects.count()
    return params
