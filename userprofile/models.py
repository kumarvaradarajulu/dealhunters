from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserProfile(models.Model):
    user_id = models.OneToOneField(User)  #  Id of the user
    title = models.CharField(max_length=100)  # Title for the user
    create_date = models.DateTimeField(blank=False)  #  Account creation date of the user
    likes = models.IntegerField(default=0)  # Number of likes
    Unlikes = models.IntegerField(default=0)  # Number of unlikes
    badges = models.CharField(max_length=50, blank=True, null=True)  # Batches the user has earned.