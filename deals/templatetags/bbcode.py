import re
from django import template
from django.utils.safestring import mark_safe
from django.utils.html import conditional_escape
from django.utils.translation import ungettext, ugettext as _
import datetime
from django.utils import timezone


register = template.Library()


@register.filter(needs_autoescape=False)
def bbcode(value, autoescape=None):

    from common.frequentfuncs import bbcode_func

    if autoescape:
        esc = conditional_escape
    else:
        esc = lambda x: x

    return mark_safe(bbcode_func(value))

@register.filter
def comment_num(comment_page, comments_per_page):
    comment_page = comment_page - 1
    return comments_per_page * comment_page

@register.filter
def get_query_string(base_url, toadd):
    if base_url.find('?') == -1:
        # There is no Query string Enjoy
        return base_url+'?%s' % toadd
    else:
        pos = base_url.find(toadd)
        if  pos == -1:
            # The new query is not there, So Enjoy
            return base_url+'&%s' % toadd
        else:
            if base_url.find('&') > -1:
                pattern1 = r'%s=.+?&' % toadd
                pattern2 = r'%s=.+?$' % toadd
                base_url = re.sub(pattern1, '', base_url)
                base_url = re.sub(pattern2, '', base_url)
                return base_url + '%s' % toadd


@register.filter
def fuzzy_date(d, now=None):
    return fuzzy_date_diff(d, now)


def fuzzy_date_diff(d, now=None):

    #d = d.replace(tzinfo=None)
    # Convert datetime.date to datetime.datetime for comparison.
    if not isinstance(d, datetime.datetime):
        d = datetime.datetime(d.year, d.month, d.day)

    if now and not isinstance(now, datetime.datetime):
        now = datetime.datetime(now.year, now.month, now.day)

    if not now:
        if d.tzinfo:
            from django.utils.tzinfo import LocalTimezone
            now = datetime.datetime.now(LocalTimezone(d))
        else:
            now = datetime.datetime.now()

    in_the_futur = d > now

    today = timezone.make_aware(datetime.datetime(now.year, now.month, now.day), d.tzinfo) if d.tzinfo else datetime.datetime(now.year, now.month, now.day)
    tomorrow = timezone.make_aware(datetime.datetime(now.year, now.month, now.day+1), d.tzinfo) if d.tzinfo else datetime.datetime(now.year, now.month, now.day+1)

    if in_the_futur:
        delta = d - now
        delta_midnight = d - tomorrow
    else:
        delta = now - d
        delta_midnight = today - d

    if delta.days == 0 and delta.seconds < 60:
        return _("just now")

    day_chunks = (
        (365.242199, lambda n: ungettext('year', 'years', n)),
        (30.4368499, lambda n: ungettext('month', 'months', n)),
        (7.0, lambda n: ungettext('week', 'weeks', n)),
        (1.0, lambda n: ungettext('day', 'days', n)),
    )

    second_chunks = (
        (3600, lambda n: ungettext('hour', 'hours', n)),
        (60, lambda n: ungettext('minute', 'minutes', n)),
    )

    if delta_midnight.days == 0:
        hours = delta_midnight.seconds / 3600.
        if in_the_futur:
            if hours < 12:
                return _("tomorrow morning")
            else:
                return _("tomorrow afternoon")
        else:
            if hours > 12:
                return _("yesterday morning")
            else:
                return _("yesterday afternoon")

    count = 0
    for i, (chunk, name) in enumerate(day_chunks):
        if delta.days >= chunk:
            count = round((delta_midnight.days + 1)/chunk, 0)
            break

    second_count = None

    for i, (second_chunk, second_name) in enumerate(second_chunks):
        if delta.seconds >= second_chunk:
            second_count = round(delta.seconds/second_chunk, 0)
            break

    if not count:
        output = '%(number)d %(type)s' % \
            {'number': second_count, 'type': second_name(second_count)}
    else:
        output = '%(number)d %(type)s, %(second_number)d %(second_type)s' % \
            {'number': count, 'type': name(count), 'second_number': second_count, 'second_type': second_name(second_count)}

    if in_the_futur:
        return _('in %s' % output)
    else:
        return _('%s ago' % output)