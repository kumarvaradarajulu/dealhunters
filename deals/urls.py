__author__ = 'kumar'

from django.conf.urls import url, patterns


urlpatterns = patterns(
    '',
    url(r'^view/(?P<deal_slug>[a-zA-Z\-_0-9]+)', 'deals.views.view', name='details'),
    url(r'^visit$', 'deals.views.visit', name='visit'),
#    url(r'^(?P<category>[\w]+)/(?P<sort>[\w]+)/$', 'deals.views.dealslist', name='catlist'),
    url(r'^(?P<category>[a-zA-z]+)/$', 'deals.views.dealslist', name='sortlist'),
    url(r'^$', 'deals.views.dealslist', name='alllist'),
)