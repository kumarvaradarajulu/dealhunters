# Create your views here.

from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect, HttpResponseBadRequest, HttpResponse, Http404
from deals.models import Deals, Comments, Stores, content_file_name, Visits
from django.contrib.auth.decorators import login_required
from common.frequentfuncs import *
from django.db.models import Count
import logging
from datetime import date, timedelta
from django.db.models.query import QuerySet
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.core.context_processors import csrf
from deals.forms import PostDealForm
from django.template.defaultfilters import slugify
from urlparse import urlparse
from django.contrib.auth.hashers import make_password
from django.contrib import auth
import datetime
from common.decors import *
from itertools import chain
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.db import transaction
from sorl.thumbnail import get_thumbnail
import urllib2
from django.core.files import File
from urlparse import urlparse
from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse
from django.utils.timezone import now

logr = logging.getLogger()


def dealslist(request, category=None):
    (args, params) = common_deal_funcs(request)
    # TODO: Logic to get category needs to be updated to get preference from cookies
    cat_num = False
    if not category:
        category = 'all topics'

    # Check if the Category Matches
    if category and category.lower() != 'all topics':
        cat_num = validate_category(category)
        if cat_num:
            params['category'] = category
            deals = Deals.objects.filter(category=cat_num).select_related('store', 'user')
        else:
            return HttpResponseBadRequest()
    else:
        params['category'] = 'All Topics'
        deals = Deals.objects.all().select_related('store_id', 'user_id')

    # Annotate and get the number of comments
    deals = deals.annotate(commentcount=Count('comments'))

    # Check if the sort order matches.
    orderby = '-date_time'
    sort = request.GET.get('sort', None)
    if sort and sort in SORT_ORDER:
        params['sort'] = sort
        if sort == 'hot':
            deals = deals.filter(expired=False)
            orderby = '-temperature'
        elif sort == 'discussed':
            orderby = '-commentcount'

    # TODO: Pagination needs to be changed as per user's preference of number of items/per page from cookie
    items_per_page = 10
    start = 0
    end = items_per_page
    if request.GET:
        try:
            page = int(request.GET['page'])
        except:
            page = 1
        start = (page - 1) * items_per_page if page > 0 else 0
        end = page * items_per_page if page > 0 else items_per_page
    else:
        page = 1

    start = 0 if start < 0 else start
    #end = end - 1 if end > 1 else end

    params['items_per_page'] = items_per_page
    params['page'] = page

    totitems = deals.count() + 1
    end = totitems if end >= totitems else end
    params['totitems'] = totitems

    deals = deals.order_by(orderby)[start:end]

    # Call the pagination common function to get the page numbers
    pagination = paginate(page, totitems, items_per_page, True)

    # Get sidebar contents
    params['sidebar'] = get_sidebar(cat_num, False)

    args['deals'] = deals
    args['params'] = params
    args['pagination'] = pagination

    output = ''
    for key, value in args.iteritems():
        if type(value) == dict:
            for key1, value1 in value.iteritems():
                if type(value1) == QuerySet:
                    for data in value1:
                        output = output + 'key1 : ' + key1 + data.title + '</br>'
                elif type(value1) == tuple or type(value1) == list:
                    pass
                else:
                    output = output +  str(value1) + '</br>'
        elif type(value) == QuerySet:
            for data in value:
                output = output + 'key : ' + key + data.title + '</br>'

    #return HttpResponse('<p>%s</p>' % output)
    return render_to_response('deals/index.html', args)


def view(request, deal_slug):
    # TODO: Currently sort order and category preference is not stored in cookies, need to enhance it later
    (args, params) = common_deal_funcs(request)

    # TODO: This logic needs to be changed to get comments/page preferencefrom cookies
    try:
        comments_per_page = params['comments_per_page'] = int(request.GET.get('cpp'))
    except:
        comments_per_page = params['comments_per_page'] = 10
    # Number of Top comments
    num_top_comments = 3
    # Get comment_page
    try:
        commentspage = params['commentspage'] = int(request.GET.get('cpage', 1))
    except:
        commentspage = params['commentspage'] = 1

    deals = Deals.objects.filter(identity=deal_slug).annotate(totcomments=Count('comments')).select_related('Store', 'User', 'Comments')
    if not deals:
        return Http404('<h2>Page not found</h2>')

    deal = deals[0]

    totcomments = deal.totcomments + 1
    totpages = int(totcomments / comments_per_page) + 1 if totcomments % comments_per_page !=0 else totcomments / comments_per_page
    if commentspage > totpages:
        return HttpResponseRedirect(reverse('index'))

    start = (commentspage - 1) * comments_per_page if commentspage > 0 else 0
    end = commentspage * comments_per_page if commentspage > 0 else commentspage
    args['category'] = category_words(deal.category)
    args['category'] = args['category'] if args['category'] else 'computers'

    set_comments = deal.comments_set
    start = 0 if start < 0 else start
    end = totcomments if end >= totcomments else end

    topcomments = set_comments.select_related('User').extra(select={'topcomms':'likes-unlikes-abusive'})
    topcomments = topcomments.order_by('-topcomms')[0:num_top_comments]
    comments = set_comments.select_related('User')
    comments = comments.order_by('date_time')[start: end]
    comment_pagination = paginate(params['commentspage'], deal.totcomments, params['comments_per_page'])

    #Get side bar
    params['sidebar'] = get_sidebar(False, False)

    args['deal'] = deal
    args['topcomments'] = topcomments
    args['comments'] = comments
    args['comment_pagination'] = comment_pagination
    args['params'] = params
    args.update(csrf(request))
    return render_to_response('deals/details.html', args)


#@transaction.commit_manually
def submit(request):
    (args, params) = common_deal_funcs(request)

    if request.method == 'POST':
        if request.user.is_authenticated():
            request.POST['email'] = request.user.email
        form = PostDealForm(request.POST, request.FILES)
        if form.is_valid():
            data = form.cleaned_data
            tags = ''
            if not request.user.is_authenticated():
                form_email = data.get('email', '')
                params['user'] = get_create_user(form_email, request)

            deal = Deals()
            deal.deal_type = data.get('deal_type')
            deal.category = data.get('category')
            deal.title = data.get('title', 'Title is empty')
            deal.identity = data.get('identity', slugify('%s-%s' % (deal.title, params['last_id']+1)))
            deal.price = data.get('price', 0)
            deal.url = data.get('url')
            image_url = data.get('image_url', '')

            image = data.get('image', '')

            store_url = urlparse(deal.url)
            urlhost = store_url.hostname.split('.')

            if len(urlhost) > 1:
                store = urlhost[-2]
            else:
                store = urlhost[-1] if len(urlhost) == 1 else ''

            deal.store = store

            try:
                store_rec = Stores.objects.get(name=store)
            except:
                store_rec = Stores()
                store_rec.name = store
                store_rec.url = store_url.scheme + '//' + store_url.netloc
                store_rec.views = 0
                store_rec.store_type = 1
                store_rec.save()

            deal.store_id = store_rec
            deal.store_address = data.get('')
            deal.description = data.get('description')
            deal.temperature = 0
            deal.start_date = data.get('from_date')
            deal.end_date = data.get('to_date')
            deal.expired = False
            deal.active = False
            deal.last_made_hot = deal.date_time = now()
            deal.user = params['user']

            tags = data.get('tags', '')
            tag_objs = get_create_tags(tags) if tags else list()
            if image:
                deal.image_url = image
                deal.save()
            elif image_url:
                get_file_from_url(deal, image_url)
            else:
                deal.image_url = ''
                deal.save()

            #transaction.commit()
            if tag_objs:
                for tag_obj in tag_objs:
                    deal_tag = DealTags()
                    deal_tag.deal_id = deal
                    deal_tag.tag_id = tag_obj
                    deal_tag.save()
            args['message'] = 'Thanks for sharing the deal. The deal will appear shortly after moderation.'

            form = PostDealForm()
    else:
        form = PostDealForm()

    #transaction.commit()
    args.update(csrf(request))
    #Get side bar
    params['sidebar'] = get_sidebar(False, False)

    args['form'] = form
    args['params'] = params
    return render_to_response('deals/submit.html', args)


def visit(request):
    (args, params) = common_deal_funcs(request)
    post_id = request.GET.get('deal','')
    if post_id and re.match(r'^[0-9]+$', post_id):
        deal = Deals.objects.filter(pk=post_id).select_related('Store')
        if not deal:
            #TODO: Decide if we need to use page HTTP404
            return HttpResponseRedirect(reverse('index'))

        deal = deal[0]
        visit = Visits()
        visit.store_id = deal.store_id
        visit.deal_id = deal
        visit.ip = hashit(get_client_ip(request), 'iphash')
        visit.user_id = request.user if request.user.is_authenticated() else ''
        visit.date_time = now()
        visit.save()
        if deal.url:
            return HttpResponseRedirect(deal.url)
    else:
        return HttpResponseRedirect(reverse('index'))


def postcomment(request):
    pass


def get_file_from_url(instance, img_url):
    name = urlparse(img_url).path.split('/')[-1]
    content = ContentFile(urllib2.urlopen(img_url).read())
    instance.image_url.save(name, content, save=True)
    return content
