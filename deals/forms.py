__author__ = 'kumar'

from django import forms
from deals.models import *
from sorl.thumbnail.fields import ImageFormField


class PostDealForm(forms.Form):
    DEAL_TYPE_ONLINE = 1
    DEAL_TYPE_INSTORE = 2
    DEAL_TYPE_BOTH = 3
    DEAL_TYPE_CHOICES = (
        (DEAL_TYPE_ONLINE, 'Online'),
        (DEAL_TYPE_INSTORE, 'In-store'),
        (DEAL_TYPE_BOTH, 'Online & Instore')
    )

    DESCRIPTION_PLACEHOLDER = 'Description of the deal goes here. Explain to the community how this is good deal and\
    how it can be availed. List Features of the product so the community can evaluate and from this deal.'
    INSTRUCTIONS_PLACEHOLDER = 'If any instructions are applicable on how to grab the deal, mention it here. \
    This shall be additional information shared for better understanding.'


    deal_type = forms.ChoiceField(choices=DEAL_TYPE_CHOICES, required=True,
                                  help_text="Deal type whether it is an Online deal or Instore Deal")
    category = forms.ChoiceField(choices=Deals.DEAL_CATEGORY, required=True)
    price = forms.DecimalField(required=True, help_text='Enter 0 if price is not applicable')
    title = forms.CharField(max_length=100, required=True,
                            widget=forms.TextInput(attrs={'placeholder': 'Enter Title of the Deal (Max 100 chars)'}),
                            help_text='Sample Title: Dell XPS 12 core i7, 8GB RAM for 82K @ dell.co.in')
    url = forms.URLField(max_length=255, required=True,
                         widget=forms.TextInput(attrs={'placeholder': 'Enter URL of the Deal in case of Online Deal'}),
                         help_text='Enter valid URL of the deal if it is an oline deal')
    description = forms.CharField(widget=forms.Textarea(attrs={'placeholder': DESCRIPTION_PLACEHOLDER}), required=True)
    tags = forms.CharField(required=False,
                           widget=forms.TextInput(attrs={'placeholder': 'Tag the deal, separate by comma for multiple tags'}),
                           help_text='Good tags help other members search and identify deals specifically.')
    coupon = forms.CharField(max_length=100, required=False,
                             widget=forms.TextInput(attrs={'placeholder': 'Enter Coupon code for the deal if any'}),
                             help_text='If coupons are to be used to get the lowest price, please mention it here')
    from_date = forms.DateTimeField(required=False, widget=forms.DateTimeInput,
                                    help_text='Date from when the deal is active')
    to_date = forms.DateTimeField(required=False, widget=forms.DateTimeInput,
                                  help_text='Date after which deal will Expire')
    instructions = forms.CharField(required=False, widget=forms.Textarea(attrs={'placeholder': INSTRUCTIONS_PLACEHOLDER}),
                                   help_text='Any additional instructions which could help community grab the deal')
    image = ImageFormField(required=False, widget=forms.FileInput)
    image_url = forms.URLField(required=False)
    email = forms.EmailField(required=True, help_text='Do not Worry, Your email is SAFE with us.')

    def clean_from_date(self):
        from_date = self.cleaned_data.get('from_date')
        to_date = self.cleaned_data.get('to_date')

        if from_date < to_date:
            raise forms.ValidationError('From date cannot be less than to date.')

        return from_date
