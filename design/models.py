__author__ = 'kumar'

from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User


class Deals(models.Model):
    """
        This table is the main table which contains details of the deal
    """

    # Constants used by the model
    # Deal type
    DEAL_TYPE_INSTORE = 0
    DEAL_TYPE_ONLINE = 1
    DEAL_TYPE_BOTH = 2
    # Deal Category
    DEAL_CATEGORY_AUDIOVIS = 1
    DEAL_CATEGORY_MOBILES = 2
    DEAL_CATEGORY_COMPUTERS = 3
    DEAL_CATEGORY_CLOTHING = 4
    DEAL_CATEGORY_GROCERY = 5
    DEAL_CATEGORY_FURNITURE = 6
    DEAL_CATEGORY_GAMING = 7
    DEAL_CATEGORY_TRAVEL = 8
    DEAL_CATEGORY_KIDS = 9
    DEAL_CATEGORY_FOOD = 10

    # Choices field
    DEAL_TYPE = (
        (DEAL_TYPE_INSTORE, 'Instore'),
        (DEAL_TYPE_ONLINE, 'Online'),
        (DEAL_TYPE_BOTH, 'Online-Instore'),
    )

    DEAL_CATEGORY = (
        (DEAL_CATEGORY_AUDIOVIS, 'Audio/Visual'),
        (DEAL_CATEGORY_MOBILES, 'Mobiles'),
        (DEAL_CATEGORY_COMPUTERS, 'Computers'),
        (DEAL_CATEGORY_CLOTHING, 'Clothing/Fashion'),
        (DEAL_CATEGORY_GROCERY, 'Groceries'),
        (DEAL_CATEGORY_FURNITURE, 'Furnitures'),
        (DEAL_CATEGORY_GAMING, 'Gaming'),
        (DEAL_CATEGORY_TRAVEL, 'Travel'),
        (DEAL_CATEGORY_KIDS, 'Kids'),
        (DEAL_CATEGORY_FOOD, 'Restaurants'),
    )

    title = models.CharField(max_length=200, db_index=True)  #   Title of the deal
    identity = models.SlugField(max_length=255)  # Slug field, combination of title and ID
    deal_type = models.IntegerField(blank=False, null=False, choices=DEAL_TYPE)  # Type like Instore or Online
    category = models.IntegerField(blank=False, null=False, choices=DEAL_CATEGORY)  # Category of the deal
    price = models.DecimalField(default=0.0)  #  Price of the deal
    url = models.URLField(blank=True, null=True)  #  URL of the deal for Online deals
    store = models.CharField(max_length=100)  #  Store Name of the deal
    store_id = models.ForeignKey(Stores)  #  Store ID (got from another table based on store name)
    store_address = models.CharField(max_length=200)  # Store Address of this particular deal
    description = models.TextField(blank=True)  # Description of the deal
    temperature = models.IntegerField(default=0)  # Hotness of the deal
    start_date = models.DateTimeField(null=True, blank=True)  # Start Date of the deal
    expiry_date = models.DateTimeField(null=True, blank=True)  # Expiry Date of the field
    expired = models.BooleanField(default=False)  # Expired flag
    active = models.BooleanField(default=False)  #  Active or deactive flag
    user = models.ForeignKey(User)  #  User who posted the deal
    date_time = models.DateTimeField(default=timezone.now())  # Date and time of posting the deal


class Stores(models.Model):
    name = models.CharField(max_length=100, db_index=True)  #  Name of the store
    store_type = models.IntegerField(max_length=20, choices=Deals.DEAL_TYPE)  # Type of the store Online or store or Both
    url = models.URLField(max_length=255, blank=True, null=True)  #  Website URL of the store
    visits = models.IntegerField(default=0)  #   Total Number of online visits to this store
    category = models.CharField(max_length=20, blank=True, null=True)  #  Category/ies of the store like (Electronics..)


class Tags(models.Model):
    name = models.CharField(max_length=30, db_index=True)  #  Name of the tag
    tagged = models.IntegerField(default=0)  # Number of time tagged


class DealTags(models.Model):
    tag_id = models.ForeignKey(Tags)  # Tag id
    deal_id = models.ForeignKey(Deals)  # deal ID


class Comments(models.Model):
    deal_id = models.ForeignKey(Deals)  # ID of the deal
    comment = models.TextField(blank=False, null=False)  # Comment
    likes = models.IntegerField(default=0)  # Likes of the comment
    unlikes = models.IntegerField(default=0)  # UnLikes of the comment
    abusive = models.IntegerField(default=0)  # Abusive reports on the comment
    visible = models.BooleanField(default=False)  # Whether the comment is visible or not.
    user = models.ForeignKey(User)  # Uer who posted the deal
    date_time = models.DateTimeField(default=timezone.now())  # Date and time of posting the deal


class Votes(models.Model):

    # CONSTANTS
    VOTES_TYPE_UP = 0
    VOTES_TYPE_DOWN = 1

    VOTES_TYPE = (
        (VOTES_TYPE_UP, 'Vote Hot'),
        (VOTES_TYPE_DOWN, 'Vote Cold'),
    )

    deal_id = models.ForeignKey(Deals)  # ID of the deal
    votetype = models.IntegerField(choices=VOTES_TYPE, blank=False, null=False)  # upvote or downvote
    ip = models.CharField(max_length=255, blank=True, null=True) # IP of the user who voted (This is a hashed value)
    user = models.ForeignKey(User)  # User voted
    date_time = models.DateTimeField(default=timezone.now())  # Time of the vote


class Visits(models.Model):
    store_id = models.ForeignKey(Stores)  # Store ID of the online visit
    deal_id = models.ForeignKey(Deals)  # Deal ID for the visit
    ip = models.CharField(max_length=255)  # Ip address of the visit (This is a hashed value)
    user = models.ForeignKey(User)  # User who visited the url (blank for guest users)
    date_time = models.DateTimeField(default=timezone.now())  # Date Time of the visit


class UserProfile(models.Model):
    user_id = models.OneToOneField(User)  #  Id of the user
    title = models.CharField(max_length=100)  # Title for the user
    create_date = models.DateTimeField(blank=False)  #  Account creation date of the user
    likes = models.IntegerField(default=0)  # Number of likes
    Unlikes = models.IntegerField(default=0)  # Number of unlikes
    badges = models.CharField(max_length=50, blank=True, null=True)  # Batches the user has earned.


class Subscriptions(models.Model):

    SUBSCR_TYPE_ALL_DAILY = 0
    SUBSCR_TYPE_ALL_WEEKLY = 1

    SUBSCR_TYPE = (
        (SUBSCR_TYPE_ALL_DAILY, 'All Posts/day'),
        (SUBSCR_TYPE_ALL_WEEKLY, 'All Posts/Weekly'),
    )

    user_id = models.ForeignKey(User)  # Id of the user
    subscr_type = models.IntegerField(blank=True, null=True, choices=SUBSCR_TYPE)  # Subscription type (Type of subscript is very important)
    deal_id = models.ForeignKey(Deals)  # Deal ID of the subscription (This will be blank for general subscriptions)
    period = models.IntegerField()  # Interval Period of subscription
    active = models.BooleanField(default=True)  #  Active or Inactive