__author__ = 'kumar'


from deals.models import *
from django.template.defaultfilters import slugify
import datetime
from django.utils import timezone
from django.contrib.auth.models import User


# Create a few Stores for testing

testuser = User.objects.get(username='kumar')

for i in range(41,51):
    store = Stores(None,
        'store%s'%i,
        Stores.DEAL_TYPE[i%3][0],
        '',
        0,
        ''
    )
    store.save()

store = Stores.objects.all()
print [(i.name,i.store_type) for i in store]

# Create a few deals for testing

for i in range(1,11):
    title = "Title of the deal %s" % i
    print i
    deal = Deals(
        None,
        title,
        slugify(title),
        Deals.DEAL_TYPE[i%3][0],
        Deals.DEAL_CATEGORY[i%9][0],
        i*122.44,
        'http://bestofdeals.co.in/%s'%i,
        '',
        'store%s'%i,
        i,
        '',
        'Description of the deal %s, should be a bigger description so it can be shown in with all \
        of the lines in a multi line format. We still have BBcodes to take care of here'%i,
        0,
        datetime.datetime(2013, 8, i+1),
        datetime.datetime(2013, 8, i+10),
        False,
        False,
        timezone.now(),
        testuser.id,
        timezone.now()
    )
    deal.save()
    deal.query()

deal = Deals.objects.all()
print [(i.title,i.identity) for i in deal]

# Create a few Comments for testing

for deal in Deals.objects.all():
    for i in range(1,11):
        comment = Comments(
            None,
            deal.id,
            'This is the comment number %s for deal %s' % (i, deal.id),
            i*deal.id+deal.id,
            i*deal.id,
            0,
            False,
            testuser.id,
            timezone.now()
        )
        comment.save()

comment = Comments.objects.all()
print [(i.comment,i.deal_id) for i in comment]