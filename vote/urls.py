__author__ = 'kumar'

from django.conf.urls import patterns, url


urlpatterns = patterns(
    url(r'^deal/(?P<votetype>[a-zA-Z])', 'vote.views.deals_vote', name='dealsvote'),
)
