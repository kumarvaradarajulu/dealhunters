# Create your views here.

from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponse

def deals_vote(votetype=None, deal_id=None):
    if votetype and deal_id and votetype.lower() in ('up', 'down'):
        return HttpResponse('Voted')
    else:
        return HttpResponseRedirect('/')