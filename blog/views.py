# Create your views here.
from common.frequentfuncs import get_sidebar, populate_categories, common_funcs
from itertools import chain
from django.shortcuts import render_to_response


def index(request):
    (args, params) = common_funcs(request)
    return render_to_response('blog.html', args)