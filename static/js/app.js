/**
 * setup JQuery's AJAX methods to setup CSRF token in the request before sending it off.
 * http://stackoverflow.com/questions/5100539/django-csrf-check-failing-with-an-ajax-post-request
 */

function getCookie(name)
{
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?

            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$(document).ready(function(){

    $(document).keyup(function(e){
       if (e.keyCode == 27){
           $('#message_box').fadeOut();
           $("#overlay").fadeOut();       }
    });
    // Add markItUp! to your textarea in one line
    // $('textarea').markItUp( { Settings }, { OptionalExtraSettings } );
    var martkitup = $('#markItUp');
    if (martkitup.is(':visible')){
	    martkitup.markItUp(mySettings);
    }
    
    // You can add content from anywhere in your page
    // $.markItUp( { Settings } );	
    $('.add').click(function() {
        $.markItUp( { 	openWith:'<opening tag>',
                        closeWith:'<\/closing tag>',
                        placeHolder:"New content"
                    }
                );
        return false;
    });
    
    // And you can add/remove markItUp! whenever you want
    // $(textarea).markItUpRemove();
    $('.toggle').click(function() {
        if ($("#markItUp.markItUpEditor").length === 1) {
            $("#markItUp").markItUpRemove();
            $("span", this).text("get markItUp! back");
        } else {
            $('#markItUp').markItUp(mySettings);
            $("span", this).text("remove markItUp!");
        }
        return false;
    });
    $(".link").click(
        function(e) {
            e.preventDefault();
            var f = $("#markItUp");
            f.val("[quote=" + "[/quote]\n");
            f.focus();
    });
    $(".post_comment").click(
        function(e) {
            e.preventDefault();
            var f = $("#markItUp");
            f.val("");
            f.focus();
    });  
    $('#login-trigger').click(function(){
            $(this).next('#login-content').slideToggle();
            $(this).toggleClass('active');                                  
    if ($('#login-trigger').hasClass('active')){
        $('#login-arrow').removeClass('small-down-arrow');
        $('#login-arrow').addClass('small-up-arrow');
    } else {
        $('#login-arrow').addClass('small-down-arrow');
        $('#login-arrow').removeClass('small-up-arrow');      	
    }
    });
    
    // Swapping Side bar
    $('.snapshot-title').click(function(){
        var id = $(this).attr('id');
        var type = id.replace("snapshot-title-","");
        var tablename = "";
        tablename = "#snapshot-table-" + type;
        
        $('.visible-table').each(function(){
            $(this).removeClass("visible-table");
            var temp = $(this).attr('id');
            temp = temp.replace("snapshot-table-","");
            $('#snapshot-title-'+temp).removeClass('selected');
        });
        $(tablename).addClass("visible-table");
        $(this).addClass('selected');        
        switch (type){
            case "1":
                text = "Hottest Today";
                break;
            case "2":
                text = "Hottest This Week";
                break;
            case "3":
                text = "Hottest This Month";
                break;
            case "4":
                text = "Hottest All Time";
                break;
            default:
                text = "Hottest Today";
                break;                            
        }
        $('#snapshot-header h5').html(text);
    });
    
    // Click on Comments page nav
    $('.comments-page-trigger').click(function(){
        $(this).parent().children('.comments-page').slideToggle().delay(200);
    });
    $('.comments-pp-trigger').click(function(){
        $(this).parent().children('.comments-pp').slideToggle().delay(200);
    });  
    
    $('.quote-reply').click(function(){
        var martkitup = $("#markItUp");
        var temp = martkitup.val();
        var value = '';
        var user = '';
        if ($(this).hasClass('post-quote')){
            var post = $('#post-content');
            value = unescape(unescape(post.html()));
            user = unescape(unescape($('#post-detail-user').html()));
        } else{
            var comparent = $(this).parent().parent().parent();
            value = unescape(unescape($(comparent).children('.comment-detail').html()));
            user = unescape(unescape($(this).parent().parent().children('.left').children('a').html()));
            //var user = userhtml.match(/.*?<a class="bold italic">(.*?)<\/a>/)[1]
        }
        value = value.replace(/(<.*?>)\n/g,'$1');
        value = value.replace(/>[\t ]+</g,'><');
        value = value.replace(/\t+/g,'');
        value = value.replace(/\n[ ]+/g,'\n');
        value = value.replace(/>[ ]+/g,'>');
        // Replce /" with "
        value = value.replace(/\\"/g,'"');
        // Remove non - chars
        value = value.replace(/[^A-Za-z0-9_\-\$%,\.\(\)\*&!@=<>""''\t \/\n]:/g,'');
        //Replace bbcode header       
        value = value.replace(/<div class=\"bbcode-quote\">\s*<div class=\"bbcode-quote-head\">(.*?)<\/div>/g,'[quote=$1]');
        // Replace bbcode body
        value = value.replace(/<div class="bbcode-quote-body">([\s\S]*?)<\/div>/g,'[bbody]$1[/bbody]');
        // Replace not required a element
        value = value.replace(/<a class="expand-quote">View full quote<\/a>/g,'');        
        value = value.replace(/<\/div>/g,'[/quote]');
        
        // Replace allowed elements with respective tags
        value = value.replace(/<b>/g,'[b]');               
        value = value.replace(/<\/b>/g,'[/b]');               
        value = value.replace(/<i>/g,'[em]');               
        value = value.replace(/<\/i>/g,'[/em]');            
        value = value.replace(/<u>/g,'[u]');               
        value = value.replace(/<\/u>/g,'[/u]');            
        
        // For non firefox
        value = value.replace(/<img.*?src="(.*?)".*?\/>/g,'[img]$1[/img]');
        //value = value.replace(/<img.*?".*?".*?src="(.*?)".*?>/g,'[img]$1[/img]');
        value = value.replace(/<img.*?(src|SRC)="(.*?)".*?>/g,'[img]$2[/img]');
        value = value.replace(/<a.*?href="(.*?)".*?>(.*?)<\/a>/gi,'[url="$1"]$2[/url]');

        // Replace multiple spaces to one space.
        value = value.replace(/\n[ \r\t]*?\n/g,'\n');
                
        // This shall be the last replacement, which replaces all other tags with spaces
        value = value.replace(/<.*?>(.*?)<\/.*?>/g,'$1');            
        
        // Replace any single tags like code comments or img tags
        value = value.replace(/<.*?>/g,'');            
        martkitup.val(temp+'[quote='+user+']'+value+'[/quote]');
        martkitup.focus();
    });

    // help text dialog
    $("div.login_reg_form input, div.login_reg_form textarea").focus(function(){
        help_id = '#'+$(this).attr('id')+'_help';
        if ($(help_id).html()){
            h = parseInt($(window).height());
            w = parseInt($(window).width());
            pos = $(this).position();
            input_v = parseInt(pos.top);
            input_h = parseInt(pos.left);
            help_v = input_v - 50;
            help_h = input_h + 80;
            // console.log(h, w);
            // console.log(input_h, input_v);
            console.log($(help_id).height(), $(help_id).width());
            help_v = (parseInt($(help_id).width()) > 339) ? help_v - 13 : help_v;
            $(help_id).css('top', help_v);
            $(help_id).css('left', help_h);
            $(help_id+'+span.help_text_border').css('top', help_v + ((parseInt($(help_id).width()) > 339) ? 46 : 33));
            $(help_id+'+span.help_text_border').css('left', help_h+10);
            $(help_id+'+span.help_text_border').css('display', 'block');
            $(help_id).css('display', 'inline-block');
        }
    });

    $("div.login_reg_form input, div.login_reg_form textarea").focusout(function(){
        $('#'+$(this).attr('id')+'_help').css('display', 'none');
        $('#'+$(this).attr('id')+'_help+span.help_text_border').css('display', 'none');
    });

    // handle submit form validations here
    /*$('#submit_form').submit(function(e){
        e.preventDefault();
       if ($('#id_deal_type').val() == ''){

       }

       if ($('#id_title').val() == ''){

       }
    });
    */

    $('#post_comment_form').submit(function(e){
        var serializeData = $('#post_comment_form').serialize();
        var comment = $('#markItUp').val();
        var markitup = $('#markItUpMarkItUp');
        var post_comm = $('#post_comment_msg');
        var wrap = $('#editor-wrap');
        wrap.removeClass('loading');
        post_comm.removeClass('success');
        post_comm.removeClass('failure');
        post_comm.addClass('visible');
        post_comm.removeClass('invisible');
        var post_type = $('#post_type').val();
        wrap.addClass('loading');
        markitup.addClass('loading');
        post_comm.html('Please wait...');
        setTimeout(function(){
        if ($('#post_email').length != 0){
            var email = $('#post_email').val();
            if (!email || !comment){
                 post_comm.html('Email and Comment must be entered');
                 post_comm.removeClass('invisible');
                 post_comm.addClass('visible');
                 post_comm.addClass('failure');
                 wrap.addClass('failure');
                 wrap.removeClass('loading');
                 markitup.removeClass('loading');
                 return false;
            }
        }

        if (!comment){
           post_comm.html('Comment must be entered');
           post_comm.removeClass('invisible');
           post_comm.addClass('visible');
           post_comm.addClass('failure');
            wrap.removeClass('loading');
            markitup.removeClass('loading');
            return false;
        }

        var urltopost = '/asja/postcomment/'+post_type+'/'+$('#post_id').val();
        $.ajax({
            type: 'POST',
            url: urltopost,
            data: serializeData,
            cache: 'false',
            dataType: 'json',
            async: 'true',

            success: function(data){
                post_comm.html(data.message);
                post_comm.removeClass('invisible');
                post_comm.addClass('visible');
                if (Boolean(data.status)){
                    post_comm.addClass('success');
                } else{
                    post_comm.addClass('failure');
                }
            }})
        wrap.removeClass('loading');
        markitup.removeClass('loading');
    }, 2000);
        return false;
    });

    $('span.vote').click(function() {
        var vote_div = $(this).parent().parent();
        var left = vote_div.offset().left;
        var top = vote_div.offset().top - 20;
        var default_msg = 'Thanks for showing interest in helping the community by voting. But in order to avoid ' +
            'scams we request you to kindly <a href="/login">login/register</a>';
        var spanel = $(this).parent().parent().children('span.hotness');
        var message_box = $('#message_box');
        var message_box_msg = $('#message_box_msg');
        message_box_msg.removeClass('failure');
        message_box_msg.removeClass('success');
        message_box_msg.removeClass('loading');
        $('#overlay').show();

        message_box.css({
             'top': top,
             'left': left
        });
        message_box.fadeIn(500);
        if ($(this).parent().hasClass('disabled')){
            message_box_msg.html(default_msg);
            message_box_msg.addClass('failure');
        } else{
            var vote_div_id = vote_div.attr('id');
            var temp = vote_div_id.split('_');
            var urltopost = '/asja/vote/' + temp[1];
            message_box_msg.html('');
            message_box.addClass('loading');
            var type = ($(this).hasClass('large-down-arrow')) ? 'down' : 'up';
            setTimeout(function(){
                $.ajax({
                    type: 'POST',
                    url: urltopost,
                    data: {
                        'vote_type': type,
                        'post_id': temp[2]
                    },
                    cache: 'false',
                    dataType: 'json',
                    async: 'true',

                    success: function(data){
                        message_box.removeClass('loading');
                        message_box_msg.html(data.message);
                        if (Boolean(data.status)){
                            message_box_msg.addClass('failure');
                        } else{
                            message_box_msg.addClass('success');
                            spanel.html(data.temp+'&deg;');
                        }
                    }
                });
            }, 2000);
        }
    });

    $('#message_box').click(function(){
       $('#message_box').fadeOut();
       $("#overlay").fadeOut();
    });

    $.ajaxSetup({
         beforeSend: function(xhr, settings) {
             if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                 // Only send the token to relative URLs i.e. locally.
                 xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
             }
         }
    });

    $('#sorting').change(function(){
        var val = $(this).val();
        window.open(window.location.pathname+'?sort='+val, "_self");
    });

});

