// ----------------------------------------------------------------------------
// markItUp!
// ----------------------------------------------------------------------------
// Copyright (C) 2011 Jay Salvat
// http://markitup.jaysalvat.com/
// ----------------------------------------------------------------------------
// Html tags
// http://en.wikipedia.org/wiki/html
// ----------------------------------------------------------------------------
// Basic set. Feel free to add more tags
// ----------------------------------------------------------------------------
var mySettings = {
	onShiftEnter:  	{keepDefault:false, replaceWith:'<br />\n'},
	onCtrlEnter:  	{keepDefault:false, openWith:'\n<p>', closeWith:'</p>'},
	onTab:    		{keepDefault:false, replaceWith:'    '},
    previewInWindow: 'width=800, height=600, resizable=yes, scrollbars=yes',
	markupSet:  [ 	
		{name:'Bold', key:'B', openWith:'(!([b]|!|<b>)!)', closeWith:'(!([/b]|!|</b>)!)' },
		{name:'Italic', key:'I', openWith:'(!([em]|!|<i>)!)', closeWith:'(!([/em]|!|</i>)!)'  },
		{name:'Underline', key:'U', openWith:'[u]', closeWith:'[/u]' },
		{separator:'---------------' },
		{name:'Quote', key:'Q', openWith:'[quote]', closeWith:'[/quote]' },		        
		{name:'Picture', key:'P', replaceWith:'[img][![Source:!:http://]!]',closeWith:'[/img]' },
		{name:'Link', key:'L', openWith:'[url="[![Link:!:http://]!]"]', closeWith:'[/url]', placeHolder:'Your text to link...' },
		{separator:'---------------' },        
		{name:'Preview', className:'preview',  call:'preview'}
	]
}

